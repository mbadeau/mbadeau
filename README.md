# Hello! :wave:

I'm Matthew Badeau, a Senior Support Engineer at GitLab. I have been with GitLab since April 2020. You can see what type of work I do by checking out [this link](https://handbook.gitlab.com/job-families/engineering/support-engineer/).

GitLab is [public by default](https://about.gitlab.com/handbook/values/#public-by-default) and you can watch what I am publically working on through my profile. Most of my work is with GitLab customers and that cannot be shared publically.

At GitLab, there is a concept of a [public README](https://about.gitlab.com/handbook/engineering/readmes/) which helps others understand what it might be like to work with you. Below is my README, with borrowed text from [the template](https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/.gitlab/issue_templates/README-template.md).

## Matthew Badeau's README

**Matthew Badeau, Senior Support Engineer**

This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to contribute to this page by opening a [merge request]. 

## About me

* I really love computers. I've been obsessed with them since I was a kid. Everything from fixing them, programming them, building them, and of course using them.
* I love technology but I don't buy the latest gadgets. I'm more practical in that aspect and I want to avoid extra e-waste.
* Right now, I'm super into 3D printing. Mostly building DIY 3D printers but I use my skills to help print spare parts for broken items around the house -- in the desire to also prevent waste. I understand that 3D printing is wasteful but I'm hoping my contributions balance that out in the future.
* 3D modelling is also fun. I often use a hobbyist license of Fusion 360 to develop models and publish them online whenever I can. That way, others can prevent items of their own from ending up in the landfill.

## How you can help me

* I'm relying on you to ask questions if I come across as too vague or I have a misunderstanding of the situation.
* I'm a little forgetful. Sometimes I need a reminder.
* Please let me know of things that might be related to what I'm doing. I'm not aware of what I'm not aware of, so let me know

## My working style

* Because of childcare reasons, I have strict working hours and I'm not able to work after a certain time.
* I sometimes get extremely focused on issues but then suddenly lose interest.

## Communicating with me

* As much as I try to keep up with my todos, I'm not always aware of what's happening. There's a lot going on. Feel free to ping me in Slack.
* I forget greetings a lot. Please don't get offended if I forget to say hello.
* I'm rarely firm on a topic. If I'm questioning it, it's not to dismiss the idea but to build off of that or answer questions.

## My WFH setup

| Item | Brand |
| ------ | ------ |
| Sit/Stand desk | [Flexispot E7](https://www.flexispot.com/flexispot-pro-standing-desk-e7)
| Dual monitor setup | BenQ PD2700U |
| Big TKL Aluminium mechanical Keyboard | WASD keyboards |
| Mouse | Logitech MX Master 3 |
| Dock | CalDigit TS4 |
| Network Adapter | Caldigit Connect 10G |
| Internet | Really fast |
